<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tipoActividadRealizar extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_actividad',
    ];

    public $timestamps = true;
}
