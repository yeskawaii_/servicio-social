<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class actividadesSolicitudExterna extends Model
{
    use HasFactory;

    protected $table = 'actividades_solicitud_externa';

    static $rules = [
        'id_solicitud' => 'integer',
        'id_actividad' => 'integer',
        'id_carrera' => 'integer',
        'numero_alumnos' => 'integer',
        'otra' => 'string|max:255'
    ];

    protected $fillable = [
        'id_solicitud',
        'id_actividad',
        'id_carrera',
        'numero_alumnos',
        'otra',
    ];
    
    public $timestamps = true;
}
