<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class actividades extends Model
{
    use HasFactory;

    protected $table = 'actividades';
    protected $primaryKey = 'id';
    public $incrementing = true;

    static $rules = [
        'nombre_actividad' => 'required|string|max:255'
    ];

    protected $fillable = [
        'nombre_actividad',
    ];

    public $timestamps = true;
}
