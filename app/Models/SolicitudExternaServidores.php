<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolicitudExternaServidores extends Model
{
    use HasFactory;

    protected $table = 'solicitud_externa_servidores';
    protected $primaryKey = 'id';
    public $incrementing = true;

    static $rules = [
        'nombre_dependencia' => 'required|string|max:250',
        'domicilio' => 'required|string|max:250',
        'telefono' => 'required|numeric|max:10',
        'responsable_dependencia' => 'required|string|max:250',
        'correo_dependencia'     => 'required|string|max:250',
        'responsable_directo_programa' => 'required|string|max:250',
        'correo_responsable_directo' => 'required|string|max:250',
        'area_departamento' => 'required|string|max:250',
        'oficina_seccion' => 'required|string|max:250',
        'nombre_programa' => 'required|string|max:250',
        'impacto_social' => 'required|string|max:250',
        'objetivo'     => 'required|string|max:250',
        'compensacion_economica' => 'double|max:8',
        'numero_beneficiarios_directos' => 'required|string|max:250',
        'numero_beneficiarios_indirectos' => 'required|string|max:250',
        'fecha_solicitud' => 'date',
        'fecha_inicio_programa' => 'required|date',
        'fecha_termino_programa' => 'required|date',
        'folio_asignado' => 'string|max:250',
        'confirmacion' => 'boolean|max:250'
    ];

    protected $fillable = [
        'nombre_dependencia',
        'domicilio',
        'telefono',
        'responsable_dependencia',
        'correo_dependencia',
        'responsable_directo_programa',
        'correo_responsable_directo',
        'area_departamento',
        'oficina_seccion',
        'nombre_programa',
        'impacto_social',
        'objetivo',
        'compensacion_economica',
        'numero_beneficiarios_directos',
        'numero_beneficiarios_indirectos',
        'fecha_solicitud',
        'fecha_inicio_programa',
        'fecha_termino_programa',
        'folio_asignado',
        'confirmacion',
    ];
}
