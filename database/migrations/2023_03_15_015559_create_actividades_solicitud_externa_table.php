<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('actividades_solicitud_externa', function (Blueprint $table) {
            //$table->id();
            $table->unsignedBigInteger('id_solicitud');
            $table-> foreign('id_solicitud')->references('id')->on('solicitud_externa_servidores');
            
            $table-> unsignedBigInteger('id_carrera');
            $table-> foreign('id_carrera')->references('id')->on('carreras');

            $table->unsignedBigInteger('id_actividad');
            $table-> foreign('id_actividad')->references('id')->on('actividades');

            $table-> integer('numero_alumnos');

            $table->string('otra')->nullable()->default('No Aplica');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('actividades_solicitud_externa');
    }
};
