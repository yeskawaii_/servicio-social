<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('solicitud_externa_servidores', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_dependencia');
            $table->string('domicilio');
            $table->string('telefono');
            $table->string('responsable_dependencia');
            $table->string('correo_dependencia');
            $table->string('responsable_directo_programa');
            $table->string('correo_responsable_directo');
            $table->string('area_departamento');
            $table->string('oficina_seccion');
            $table->string('nombre_programa');
            $table->string('impacto_social');
            $table->string('objetivo');
            $table->double('compensacion_economica')->nullable();
            $table->integer('numero_beneficiarios_directos');
            $table->integer('numero_beneficiarios_indirectos');
            $table->date('fecha_solicitud');
            $table->date('fecha_inicio_programa');
            $table->date('fecha_termino_programa');
            $table->string('folio_asignado');
            $table->boolean('confirmacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('solicitud_externa_servidores');
    }
};
