<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\actividades;
use App\Models\carreras;
use App\Models\tipoActividadRealizar;
use Database\Seeders\actividadesTableSeeder as SeedersActividadesTableSeeder;
use Database\Seeders\carrerasTableSeeder as SeedersCarrerasTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'julio',
            'email' => 'bono.10@live.com.mx',
            'password' => Hash::make('12345678')
        ]);

        //
        
        $this-> call(SeedersCarrerasTableSeeder::class);
        $this-> call(SeedersActividadesTableSeeder::class);
    }
}

class carrerasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        carreras::create([
            'nombre_carrera' => 'Administración',
        ]);

        carreras::create([
            'nombre_carrera' => 'Contabilidad',
        ]);

        carreras::create([
            'nombre_carrera' => 'Bioquímica',
        ]);

        carreras::create([
            'nombre_carrera' => 'Eléctrica',
        ]);

        carreras::create([
            'nombre_carrera' => 'Electrónica',
        ]);

        carreras::create([
            'nombre_carrera' => 'Industrial',
        ]);

        carreras::create([
            'nombre_carrera' => 'Biomédica',
        ]);

        carreras::create([
            'nombre_carrera' => 'Materiales',
        ]);

        carreras::create([
            'nombre_carrera' => 'Mecánica',
        ]);

        carreras::create([
            'nombre_carrera' => 'Sistemas',
        ]);

        carreras::create([
            'nombre_carrera' => 'Mecatrónica',
        ]);

        carreras::create([
            'nombre_carrera' => 'IGE',
        ]);
    }
}

class actividadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        actividades::create([
            'nombre_actividad' => 'Apoyo Administrativo',
        ]);

        actividades::create([
            'nombre_actividad' => 'Apoyo a la Investigación',
        ]);

        actividades::create([
            'nombre_actividad' => 'Apoyo a Laboratorio',
        ]);

        actividades::create([
            'nombre_actividad' => 'Atención a Usuarios/Clientes',
        ]);

        actividades::create([
            'nombre_actividad' => 'Cuidado al Medio Ambiente',
        ]);

        actividades::create([
            'nombre_actividad' => 'Desarrollo Comunitario',
        ]);

        actividades::create([
            'nombre_actividad' => 'Desarrollo de Sistemas/Software',
        ]);

        actividades::create([
            'nombre_actividad' => 'Educación para Adultos',
        ]);

        actividades::create([
            'nombre_actividad' => 'Elaboración de Proyectos',
        ]);

        actividades::create([
            'nombre_actividad' => 'Programas de Contingencia',
        ]);

        actividades::create([
            'nombre_actividad' => 'Mantenimiento de Equipo e Instalaciones',
        ]);

        actividades::create([
            'nombre_actividad' => 'Promoción Cultural',
        ]);

        actividades::create([
            'nombre_actividad' => 'Promoción Deportiva',
        ]);

        actividades::create([
            'nombre_actividad' => 'Otras: (especifique)',
        ]);
    }
}