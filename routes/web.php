<?php

use App\Http\Controllers\SolicitudExternaServidoresController;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('SolicitudExternaServidores',SolicitudExternaServidoresController::class);
/*
Route::get('/solicitud-externa-servidores', function () {
    return view('/instancias/solicitud-externa-servidores');
});

Route::get('/solicitud-externa-generada', function () {
    return view('/instancias/solicitud-externa-generada');
});
*/

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/solicitud-externa-servidores', function () {
        return view('/instancias/solicitud-externa-servidores');
    })->name('solicitud-externa-servidores');

    Route::get('/solicitud-interna-servidores', function () {
        return view('/instancias/solicitud-interna-servidores');
    })->name('solicitud-interna-servidores');

    Route::get('/solicitud-externa-generada', function () {
        return view('/instancias/solicitud-externa-generada');
    })->name('solicitud-externa-generada');
    
    Route::get('/solicitud-interna-generada', function () {
        return view('/instancias/solicitud-interna-generada');
    })->name('solicitud-interna-generada');

    Route::post('solicitud-externa-servidores', [SolicitudExternaServidoresController::class, 'store'])->name('solicitud-externa-ss');
    
    Route::post('solicitud-interna-servidores', [SolicitudInternaServidoresController::class, 'store'])->name('solicitud-interna-ss');
});
