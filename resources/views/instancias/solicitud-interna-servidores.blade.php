<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Solicitud Interna') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding: 5vh 10vh">

            <form action="{{ route('solicitud-interna-ss') }}" method="post">
                @csrf
 
                    @if ($errors->any())
                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                    @endif

                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="nombre_de_la_unidad_administrativa">
                                Nombre de la unidad administrativa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="nombre_de_la_unidad_administrativa" name="nombre_de_la_unidad_administrativa" type="text" placeholder="(Departamento/División/Centro/Subdirección)">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="requiere_alumnos_itm">
                            Requiere que los servidores sociales sean necesariamente alumnos del ITM
                            </label>
                            <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="requiere_alumnos_itm" name="requiere_alumnos_itm">
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div> 
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="cantidad_alumnos_otra_institucion">
                            Si pueden ser de otra institución indique cantidad
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="cantidad_alumnos_otra_institucion" name="cantidad_alumnos_otra_institucion" type="number" value="0">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="carrera_alumnos_otra_institucion">
                                Si pueden ser de otra institución indique carrera o perfil
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="carrera_alumnos_otra_institucion" name="carrera_alumnos_otra_institucion" type="text" placeholder="Carrera o perfil">
                        </div>

                        <hr>

                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="nombre_responsable_unidad_administrativa">
                                Nombre del responsable directo de la Unidad Administrativa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="nombre_responsable_unidad_administrativa" name="nombre_responsable_unidad_administrativa" type="text" placeholder="(Jefe de Depto./Jefe de División/Jefe de Centro/Subdirector)">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="correo_responsable_unidad_administrativa">
                                Correo electrónico del responsable de la Unidad Administrativa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="correo_responsable_unidad_administrativa" name="correo_responsable_unidad_administrativa" type="text" placeholder="Correo del responsable">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="nombre_responsable_del_programa">
                                Nombre del responsable directo del programa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="nombre_responsable_del_programa" name="nombre_responsable_del_programa" type="text" placeholder="Nombre completo">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="correo_responsable_del_programa">
                                Correo electrónico del responsable directo
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="correo_responsable_del_programa" name="correo_responsable_del_programa" type="text" placeholder="Correo del responsable">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="cargo_del_responsable_del_programa">
                                Indique el cargo del responsable directo del programa
                            </label>
                            <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="cargo_del_responsable_del_programa" name="cargo_del_responsable_del_programa">
                                <option value="0">Jefe de Oficina</option><option value="1">Jefe de Proyecto</option>
                                <option value="3">Coordinador</option><option value="4">Presidente de Academia</option><option value="5">Profesor</option>
                                <option value="6">Investigador</option>
                            </select>
                        </div>

                        <hr>

                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="nombre_del_programa">
                                Nombre del programa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="nombre_del_programa" name="nombre_del_programa" type="text" placeholder="Nombre del programa">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="objetivo">
                                Objetivo
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="objetivo" name="objetivo" type="text" placeholder="Descripcion de sus objetivos">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="meta_del_poa">
                                Meta del POA a la que contribuye el programa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="meta_del_poa" name="meta_del_poa" type="text" placeholder="Meta del POA">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class = "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="impacto_social">
                                Impacto social
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="impacto_social" name="impacto_social" type="text" placeholder="Impacto social">
                        </div>

                        <hr>

                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="numero_beneficiarios_directos">
                                Numero de beneficiarios directos
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="numero_beneficiarios_directos" name="numero_beneficiarios_directos" type="number" value="0">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="numero_beneficiarios_indirectos">
                                Numero de beneficiarios indirectos
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="numero_beneficiarios_indirectos" name="numero_beneficiarios_indirectos" type="number" value="0">
                        </div>

                        <hr>

                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="fecha_llenado_solicitud">
                                Fecha de llenado de la solicitud
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="fecha_llenado_solicitud" name="fecha_llenado_solicitud" type="date">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="fecha_inicio_programa">
                                Fecha de inicio del programa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="fecha_inicio_programa" name="fecha_inicio_programa" type="date">
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="fecha_termino_prorgama">
                                Fecha del termino del programa
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="fecha_termino_prorgama" name="fecha_termino_prorgama" type="date">
                        </div>
                    </div>

                    <x-button>
                        Dar de alta
                    </x-button>
                  </form>

            </div>
        </div>
    </div>
</x-app-layout>